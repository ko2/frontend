import { Col, Row } from "react-bootstrap";

export default function NotFound() {
  return (
    <Row>
      <Col>
        <div className="pb-4" />
        Not found - 404
      </Col>
    </Row>
  );
}
